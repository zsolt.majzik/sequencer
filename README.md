# sequencer

## Install

    git clone
    pip install .

## How to use
    from insilico import signal

    # Generate a numpy array with simulated sequencing data
    arr = signal("ATCG")
