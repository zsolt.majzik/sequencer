#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <vector>
#include "signal_generator.hpp" // NOLINT build/include_subdir]

namespace py = pybind11;


py::array_t<int> gen_signal_wrapper(std::string seq) {
    params p;
    p.sequence = seq;
    rtn Vec(p);

    std::vector<int> result = Vec.vector();

    return py::array_t<int>({Vec.size()},  // shape
                          {sizeof(int)},  // stride
                          Vec.data());  // data pointer
}


PYBIND11_MODULE(insilico, m) {
    m.doc() = "Generate simulated data based on input sequence.";
    m.def("signal", &gen_signal_wrapper, "Insilico sequencing data.");
}

