/**
 * @file matrix.h
 * @author Zsolt Majzik (zsolt.majzik@gmail.com)
 * @brief Create a simple matrix class with an std::vector. 
 * The array is contigious in memory.
 * 
 * How to use example:
 *  matrix<int> m(5, 5);
 *  m[0][0] = 1;
 *  
 * @date 2022-05-22
 */
#ifndef SRC_MATRIX_HPP_
#define SRC_MATRIX_HPP_
#include <vector>

template<typename Iterator>
struct range {
    Iterator b, e;
    Iterator begin() const { return b; }
    Iterator end() const { return e; }
    std::size_t size() const { return end()-begin(); }
    bool empty() const { return begin() == end(); }
};

template<typename T>
struct span:range<T*> {
    T& operator[](std::size_t i) const {
        return this->begin()[i];
    }

    span(T* start, T* finish): range<T*> { start, finish } {}
    span(): span(nullptr, nullptr) {}
    span(T* start, std::size_t length): span(start, start+length) {}
};

template <typename T>
class matrix {
 public:
    typedef T value_type;
    // Constructors
    matrix():matrix(0, 0) {}
    explicit matrix(std::size_t n): matrix(n, n) {}
    matrix(std::size_t m, std::size_t n):
        m_size(m),
        n_size(n),
        data(m*n, 0)
        {}

    value_type& operator()(size_t m, size_t n) {
        return data[m*n_size + n];
    }

    value_type const& operator()(size_t m, size_t n) const {
        return data[m*n_size + n];
    }

    span<T> operator[](std::size_t m) {
        return {std::addressof(data[m*n_size]), n_size};
    }

    span<T const> operator[](std::size_t m) const {
        return {std::addressof(data[m*n_size]), n_size};
    }

 private:
    std::size_t m_size = 0;
    std::size_t n_size = 0;
    std::vector<T> data;
};

#endif  // SRC_MATRIX_HPP_
