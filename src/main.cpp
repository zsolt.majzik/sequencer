#include <pybind11/stl.h>
#include <pybind11/pybind11.h>
#include <pybind11/embed.h>
#include <map>
#include <iostream>
#include "matrix.hpp" // NOLINT build/include_subdir]

int main() {
    matrix<int> m(5, 5);

    m[0][0] = 10;

    std::cout << m[0][0] << std::endl;

    return 0;
}
