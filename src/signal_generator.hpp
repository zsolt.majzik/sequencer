#ifndef SRC_SIGNAL_GENERATOR_HPP_
#define SRC_SIGNAL_GENERATOR_HPP_

#include <random>
#include <vector>
#include <map>
#include <string>
#include <array>

/**
 * @brief Struct containing sequencing paramters
 *
 */
struct params {
    /**
     * map structure [base] {params} where params
     * {pulse height (Poisson rate), pulse width, interpulse duration}
     */
    std::map<std::string, std::array<float, 3>> signal;
    std::string sequence;  // DNA sequence
    int baseline;  // Baseline level (Poisson rate),
    float sampling_rate;  // Sampling rate

    params() {
        signal["A"] = {414, 0.133, 0.77};
        signal["T"] = {138, 0.091, 0.67};
        signal["G"] = {286, 0.117, 0.96};
        signal["C"] = {219, 0.096, 0.79};
        sequence = {"GCTAGCCTAGCGCTCACTGTGCT"};
        baseline = 50;
        sampling_rate = 100.0;
    }
};

/**
 * @brief Five (A,T,G,C & baseline) state Random telegraph noise generator.
 *        Time between state transitions is exponentially distributed.
 *        The signal levels are drawn from a Poisson distribution and
 *        the signal height is determined by of rate constant.
 * 
 * @param mParams struct containing kinetic and sequencing parameters.
 */
class rtn {
 public:
    // Variables and data
    params mParams;

    // Constructors
    explicit rtn(const params &in_params);

    // Data operations
    int const &operator[](size_t i) const {
        return mVector[i];
    }
    size_t size() { return mVector.size(); }
    int *data() { return mVector.data(); }
    std::vector<int> vector() { return mVector; }

 protected:
    std::vector<int> mVector;

    // Class functions
    virtual void fill();
    void gen_signal(float w, float h);
    int poisson_rand(int rate);
    virtual double exp_rand(float lambda);
};

#endif  // SRC_SIGNAL_GENERATOR_HPP_
