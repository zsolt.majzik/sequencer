#include <math.h>
#include <chrono> // NOLINT [build/c++11]
#include <iostream>
#include "signal_generator.hpp" // NOLINT build/include_subdir]

/**
 * @brief Constructors for a new rtn::rtn vector object
 * 
 */
rtn::rtn(const params &in_params): mParams(in_params) {
    gen_signal(10, mParams.baseline);
    fill();
    gen_signal(10, mParams.baseline);
}

/**
 * @brief Fill the vector with random telegraph noise.
 * 
 */
void rtn::fill() {
    std::string seq = mParams.sequence;

    for (const char &_s : seq) {
        std::string s(1, _s);

        int PH = mParams.signal[s][0];  // pulse height (count)
        float PW = mParams.signal[s][1];  // pulse width (s)
        float IPD = mParams.signal[s][2];  // inter pulse duration (s)

        // Balseine state
        gen_signal(IPD, mParams.baseline);
        // Pulse state
        gen_signal(PW, PH);
    }
}

/**
 * @brief Generate singal with Poisson noise.
 * 
 * @param w Inverse rate constat of the exponential distribution, from which
 *          random length are drawn.
 * @param h Pulse hight, used as Poisson rate
 */
void rtn::gen_signal(float w, float h) {
        double len_s = exp_rand(1/w);
        size_t len = static_cast<size_t>(ceil(len_s*mParams.sampling_rate));

        for (size_t i = 0; i < len; i++) {
            mVector.push_back(poisson_rand(h));
        }
}

/**
 * @brief Draw a number from a exponential distribution.
 * 
 * @param lambda    rate constant of the exponential distribution.
 * @return          double random number
 */
double rtn::exp_rand(float lambda) {
    unsigned seed = \
        std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine generator(seed);
    std::exponential_distribution<double> distribution(lambda);

    return distribution(generator);
}

/**
 * @brief Draw a number from a Poisson distribution.
 * 
 * @param rate of Poisson distribution.
 * @return int 
 */
int rtn::poisson_rand(int rate) {
    unsigned seed = \
        std::chrono::system_clock::now().time_since_epoch().count();

    std::default_random_engine generator(seed);
    std::poisson_distribution<int> distribution(rate);

    return distribution(generator);
}
