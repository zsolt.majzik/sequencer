.PHONY: build
.PHONY: test

lint:
	cpplint --recursive test/ src/

modules:
	git submodule update --init --recursive

config:
	mkdir -p $(CURDIR)/build
	cd $(CURDIR)/build
	cmake -B$(CURDIR)/build -S$(CURDIR) -DCMAKE_BUILD_TYPE=Debug
	cd $(CURDIR)/build

build:
	make -C $(CURDIR)/build -j8

test:
	ctest --test-dir $(CURDIR)/build -V

clean:
	rm -rf $(CURDIR)/build
	rm -rf $(CURDIR)/insilico.egg-info