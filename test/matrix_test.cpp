#include <gtest/gtest.h>
#include "../src/matrix.hpp"

struct matrix_Test : public testing::Test {
    matrix<int>* p_matrix;

    matrix_Test() {
        p_matrix = new matrix<int>(5, 5);
    }

    ~matrix_Test() {
        delete p_matrix;
        p_matrix = nullptr;
    }
};

TEST_F(matrix_Test, set_value) {
    int test_val = 5;
    (*p_matrix)(0, 0) = test_val;

    EXPECT_EQ(test_val, (*p_matrix)[0][0]);
}
